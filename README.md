# Puzzle App
The classic 15-puzzle game implemented for web and mobile using HTML, CSS and JavaScript.


# Deploy the NodeJS application using Docker
```bash
root:~# cd mobcomp_puzzleapp
root:~# docker build -t puzzle-app .
root:~# docker run -d -p 3000:3000 puzzle-app
```