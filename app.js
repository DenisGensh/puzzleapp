const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const app = express();


app.use(express.static('public'));
app.use(bodyParser.urlencoded());

app.set('view engine', 'ejs');
app.set('views', 'views');

const imagePaths = [
    'images/spongebob.png',
    'images/rick-and-morty.png',
    'images/dragon.png',
    'images/classic_puzzle.png',
    'images/lionking.png',
    'images/minecraft-sword.png',
    'images/gomer-simpson.png',
    'images/peppa-pig.png'
  ];


app.get('/', (req, res, next) => {
    let desktop = !req.headers['user-agent'].includes('Android');
    res.render('index.ejs', {
        imagePaths: imagePaths,
        desktop: desktop,
    });
});

app.get('/test', (req, res, next) => {
    res.render('test.ejs');
})

app.listen(3000);