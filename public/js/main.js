$(document).ready(function() {
  adaptSize();
});

function adaptSize() { 
  var screenWidth = $(window).width();
  var screenHeight = $(window).height();

  var canvasWidth = 320;

  if (screenWidth > 500) {
    screenWidth = 500;
  }

  $('#content').css({
    width: screenWidth,
    height: screenHeight,
  });

  var canvasMargin = (screenWidth - canvasWidth) / 2;
  $("#canvas").css( { marginLeft : canvasMargin } );
}

class ImagePanel {
    constructor(index, image) {
        this.index = index;
        this.image = image;
    }
}

let images = [];
let selectedImage = 'images/dragon.png';

const panelsCount = 16;
const dimension = 4;

var gameArray;
var rightArray;

const nullCell = new ImagePanel(0, null);

let moves = 0;

const imageWidth = 320;
const imageHeight = 320;

const panelSize = 80;

let canvas = document.querySelector('#canvas');
let ctx = canvas.getContext('2d');;

let hiddencanvas = document.querySelector('#hiddencanvas');
let hiddenctx = hiddencanvas.getContext('2d');

let greatestX = 0;
let greatestY = 0;
let greatestZ = 0;

let reachedMax = false;
let reachedCount = 0;


function initApp() {
    canvas.width = imageWidth;
    canvas.height = imageHeight;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    

    initArray();
    addShaker();
    updateMoves();

    canvas.onclick = (e) => {
      const x = (e.pageY - canvas.offsetTop)  / panelSize | 0;
      const y = (e.pageX - canvas.offsetLeft) / panelSize | 0;
      // console.log(x + ' ' + y);
      handleEvent(x, y);
    };

    canvas.ontouchend = (e) => {
      const x = (e.touches[0].pageY - canvas.offsetTop)  / panelSize | 0;
      const y = (e.touches[0].pageX - canvas.offsetLeft) / panelSize | 0;
      // console.log(x + ' ' + y);
      handleEvent(x, y);
    };

    startGame('http://localhost:3000/images/spongebob.png');
  }

function startGame(url) {
    moves = 0;
    const splitted = url.split('/');
    selectedImage = splitted[splitted.length - 2] + '/' + splitted[splitted.length - 1];

    updateMoves();
    cropImage();
    // It doesn't work without timeout sometimes.
    // The black empty canvas will be displayed
    setTimeout(() => draw(), 100);
}

function initArray() {
    gameArray = Array(dimension);
    rightArray = Array(dimension);

    for (let i = 0; i < dimension; i++) {
      gameArray[i] = Array(dimension);
      rightArray[i] = Array(dimension);
    }
}

function handleEvent(x, y) {
    move(x, y, true);
    updateMoves();
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    draw();
    if (victory()) {
      if (confirm('Congratulations!\nSolved in ' + moves + ' moves. \nOne more time?')) {
        mix(300);
        
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        moves = 0;
        updateMoves();
      }     
      draw();
    }
}

// This function crops the image in 16 pieces, which will be saved in gameArray
// This is made with hidden canvas element
function cropImage() {
    const image = new Image();
    image.src = selectedImage;

    let sx = 0;
    let sy = 0;

    let i = 0;
    let j = 0;

    for (let counter = 0; counter < panelsCount; counter++) {
      // draw a particular part of the image to be saved later
      hiddenctx.drawImage(image, sx, sy, panelSize, panelSize, 0, 0, panelSize, panelSize);

      sx += panelSize;

      // Full tow. Switch to the next
      if (sx > (imageWidth - panelSize)) {
        sy += panelSize;
        sx = 0;
      }

      const partOfImage = new Image(panelSize, panelSize);
      partOfImage.src = hiddencanvas.toDataURL(); // Save the image part
      // It might cause problems with origin, when the page is not served on the server but is opened locally
      const imagePanel = new ImagePanel(counter + 1, partOfImage);

      gameArray[i][j] = imagePanel;
      rightArray[i][j] = imagePanel;
      j++;
      if (j % 4 === 0) {
        j = 0;
        i++;
      }
    }
    
    // Last (16th) piece is empty cell
    gameArray[dimension - 1][dimension - 1] = nullCell;
    rightArray[dimension - 1][dimension - 1] = nullCell;
  }

function incrMoves() {
    moves += 1;
}

// Draw image by given coordinates
function cellView(x, y) {
    if (gameArray[x][y].image !== null) {
      ctx.drawImage(gameArray[x][y].image, y * panelSize, x * panelSize);
    }
}

// Get coordinates of the empty cell
function getNullCell() {
    for (let i = 0; i < dimension; i++) {
      for (let j = 0; j < dimension; j++) {
        if (gameArray[i][j].index === 0) {
          return {x: i, y: j};
        }
      }
    }
  }

// Draw entire image without empty cell
function draw() {
    for (let i = 0; i < dimension; i++) {
      for (let j = 0; j < dimension; j++) {
        if (gameArray[i][j].index > 0) {
          cellView(i, j);
        }
      }
    }
    drawNullCell();
}

function drawNullCell() {
    ctx.fillRect(getNullCell().y * panelSize, getNullCell().x * panelSize, panelSize, panelSize);
}

// Check if the move for the cell is possible
// Mix function uses move, but might not increment step count. Therefore incr boolean as parameter
function move(x, y, incr) {
    const nullX = getNullCell().x;
    const nullY = getNullCell().y;
    if (((x - 1 === nullX || x + 1 === nullX) && y === nullY) || // up down
      ((y - 1 === nullY || y + 1 === nullY) && x === nullX)) { // left right
      gameArray[nullX][nullY] = gameArray[x][y];
      gameArray[x][y] = nullCell;
      if (incr) {
        incrMoves();
      }
    }

}

function updateMoves() {
    document.querySelector('#movesCounter').innerHTML = 'Moves: ' + moves;
}

// Check victory configuration
function victory() {
    const victoryConfiguration = [
      [1, 2, 3, 4],
      [5, 6, 7, 8],
      [9, 10, 11, 12],
      [13, 14, 15, 0]
    ];
    // It is impossible to win in less than 5 steps. (It can be any other digit)
    let result = moves > 5 ? true : false;
    for (let i = 0; i < dimension; i++) {
        for (let j = 0; j < dimension; j++) {
          if (victoryConfiguration[i][j] !== gameArray[i][j].index) {
            result = false;
          }
        }
      }
    return result;
  }

function getRandomBool() {
    if (Math.floor(Math.random() * 2) === 0) {
      return true;
    }
  }

// Mix up the puzzle
// The half of all possible random configurations (16!) are not solvable
// For this reason it will be randomly mixed from start configuration. Like in real life
function mix(stepCount) {
    let x;
    let y;
    for (let i = 0; i < stepCount; i++) {
      const nullX = getNullCell().x;
      const nullY = getNullCell().y;
      const hMove = getRandomBool();
      const upLeft = getRandomBool();
      if (!hMove && !upLeft) { y = nullY; x = nullX - 1; }
      if (hMove && !upLeft)  { x = nullX; y = nullY + 1; }
      if (!hMove && upLeft)  { y = nullY; x = nullX + 1; }
      if (hMove && upLeft)   { x = nullX; y = nullY - 1; }
      if (0 <= x && x <= 3 && 0 <= y && y <= 3) {
        move(x, y, false);
      }
    }
    draw();
}

// Function that mix the puzzle when shaking the phone
function addShaker() {   
    window.ondevicemotion = function(event) { 
        var ay = event.accelerationIncludingGravity.y
        if (ay > 20 && !reachedMax) {
            greatestY = ay;
            reachedMax = true;
            moves = 0;
            updateMoves();
            mix(50);
            setTimeout(toggleReachedMax, 1000);
            reachedCount += 1;
        }
    }
    function toggleReachedMax() {
        reachedMax = false;
    }
}

// Hard code the victory configuration 
function giveUp() {
    gameArray[0][0] = new ImagePanel(1, findElementByIndex(1).image);
    gameArray[0][1] = new ImagePanel(2, findElementByIndex(2).image);
    gameArray[0][2] = new ImagePanel(3, findElementByIndex(3).image);
    gameArray[0][3] = new ImagePanel(4, findElementByIndex(4).image);

    gameArray[1][0] = new ImagePanel(5, findElementByIndex(5).image);
    gameArray[1][1] = new ImagePanel(6, findElementByIndex(6).image);
    gameArray[1][2] = new ImagePanel(7, findElementByIndex(7).image);
    gameArray[1][3] = new ImagePanel(8, findElementByIndex(8).image);

    gameArray[2][0] = new ImagePanel(9, findElementByIndex(9).image);
    gameArray[2][1] = new ImagePanel(10, findElementByIndex(10).image);
    gameArray[2][2] = new ImagePanel(11, findElementByIndex(11).image);
    gameArray[2][3] = nullCell;


    gameArray[3][0] = new ImagePanel(13, findElementByIndex(13).image);
    gameArray[3][1] = new ImagePanel(14, findElementByIndex(14).image);
    gameArray[3][2] = new ImagePanel(15, findElementByIndex(15).image);
    gameArray[3][3] = new ImagePanel(12, findElementByIndex(12).image);

    moves = 999;
    updateMoves();

    draw();
  }

function findElementByIndex(index){
    for (let i = 0; i < dimension; i++) {
      for (let j = 0; j < dimension; j++) {
        if (rightArray[i][j].index === index) {
          return rightArray[i][j];
        }
      }
    }
  }

function clearCanvas() {
  ctx.clearRect(0, 0, imageWidth, imageHeight);
}